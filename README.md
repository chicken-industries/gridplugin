# Grid Plugin

[[_TOC_]]

This Grid Plugin for Unreal Engine 4 offers a expandable solution for implementing gameplay grids. Currently it contains several structured grids

* Square Grid
* Hexagon Grid
* Ring Grid (squares bend to a ring arc)

A structured grid is able to define a tile just by it's index.

Unstructured approaches are WIP.

## Dependencies and Installation

Simply clone this repo in addition to the [ChickenMathLibrary](https://gitlab.com/chicken-industries/ChickenMathLibrary) in the `Plugins` folder of your project folder. Compile and have fun!



## Path Finding

Currently there is a A* implementation for the path finding. In Blueprints/C++ you can call `ShortestPath` on any `AStructuredGridBase`

![PathfindingHex](Docu/Images/Pathfinding_HexGrid.png)

Tiles can be blocked as seen in the picture in red. The shortest path from the green tile to the mouse cursor is blue.

Various grid metrics can be defined, e.g., on the square grid we can choose between a cartesian distance approximation or a manhattan metric.

![PathfindingSquare](Docu/Images/Pathfinding_SquareGrid.png)

![PathfindingSquareManhattan](Docu/Images/Pathfinding_SquareGrid_ManhattanMetric.png)

On the ring grid we can specify the arc length of the full grid up to 360 degrees. Independent of that we can define if the grid should treat borders in circular direction as periodic.

![DetailsRing](Docu/Images/DetailsPanel_RingGrid.png)

![PathfindingRing](Docu/Images/Pathfinding_RingGrid.png)

![PathfindingRingPartial](Docu/Images/Pathfinding_RingGrid_PartialRing.png)

## Range

We can call `GetTilesInRange` also depending on the grid metric.

![TilesInRangeHex](Docu/Images/TilesInRange_HexGrid.png)


## Height Map

It is possible to apply a height map to the grid, which is applied to any vertex of the grids visual representations by overriding 
`virtual float AStructuredGridBase::GetHeight(FVector2D const& Coords) const`. There is an example derived class in the project:

```C++
inline float AHexagonGridWithHeightMap::GetHeight(FVector2D const& Coords) const
{
	return 100.f * FMath::Sin(0.001f * Coords.X) * FMath::Cos(0.02f * Coords.Y);
}
```

![HeightMapHex](Docu/Images/HeightMap_HexGrid.png)

## Grid Resolution

If the height map is not well resolved or we want a curved grid, we can increase the resolution of the meshes with the `Grid Refinement Level` option in the details panel.

![DetailsPanelRefinement](Docu/Images/DetailsPanel_RingGrid_HighlightedRefinement.png)

A subdivide algorithm is applied, which can produce arbitrary fine meshes for given squares or triangles. Refinements are shown for `Grid Refinement Level = 0, 1, 2`

![GridRefinementRing0](Docu/Images/GridRefinement_RingGrid0.png)

![GridRefinementRing1](Docu/Images/GridRefinement_RingGrid1.png)

![GridRefinementRing2](Docu/Images/GridRefinement_RingGrid2.png)


## WIP 

A not so trivial example for a grid is the Geodesic Polyhedra. WIP!

![GeodesicPolyhedra](Docu/Images/WIP_GeodesicPolyhedra.png)
