#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "GridBase.generated.h"

UCLASS(abstract)
class GRIDPLUGIN_API AGridBase : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	class USceneComponent* CustomSceneRoot;
	
	// This is typically the edge size of a given tile shape
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 0.01f))
	float GridScale = 100.f;

	// Use this manipulate the material of the grid lines
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	class UProceduralMeshComponent* GridLineMesh;

	// Line thickness is relatively to the cell size [0,1].
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 0))
	float GridLineThickness = 0.05f;

	// Subdivide each Triangle "GridRefinementLevel" times. Should only be used for curvilinear meshes or with high frequent
	// heightmap function
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 0))
	int GridRefinementLevel = 0;

	// This is used to calculate hits (mouse clicks) on the grid surface. 
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	class UProceduralMeshComponent* TilesCollisionMesh;
	
	// This component can be used to create meshes on runtime, e.g., to highlight specific tiles.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	class UProceduralMeshComponent* HighlightMeshes;

	// Show the coordinates of the tiles. Only works in play mode, because the debug strings can be only printed on a HUD of a
	// player controller.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool ShowGridLabels;

protected:
	int TilesNum;

public:
	AGridBase();

	void PostActorCreated() override;

	// careful! Local mesh gets invalidated (for better performance)
	static void AddLocalToGlobalMesh(TArray<FVector>& GlobalVertices, TArray<int>& GlobalTriangleIDs,
		TArray<FVector> const& LocalVertices, TArray<int>& LocalTriangleIDs);

	void Tick(float DeltaSeconds) override;

#if WITH_EDITOR
	void PostEditChangeProperty(struct FPropertyChangedEvent& Event) override;
#endif

public:
	void BeginPlay() override;

	virtual void UpdateTilesDataAndMeshes();

	void DrawAllMeshes();

private:
	void DrawGridLinesMesh();
	void DrawTilesMeshes();

protected:
	virtual void InitTilesArray() PURE_VIRTUAL(AGridBase::InitTilesArray, ;);
	virtual void RaiseGridMeshSlightlyAboveTileMesh();

	virtual void CalcGridLineTriangulation(int TileNumber, TArray<FVector>& Vertices, TArray<int>& TriangleIDs) const
		PURE_VIRTUAL(AGridBase::CalcGridLineTriangulation, ;);
	virtual void CalcTileTriangulation(int TileNumber, TArray<FVector>& Vertices, TArray<int>& TriangleIDs) const
		PURE_VIRTUAL(AGridBase::CalcTileTriangulation, ;);

	virtual void DebugPrintGridLabels();
};
