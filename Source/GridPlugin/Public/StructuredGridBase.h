#pragma once

#include "CoreMinimal.h"
#include "GridBase.h"
#include "Math/IntPoint.h"
#include "UObject/Interface.h"

#include "StructuredGridBase.generated.h"

class AStructuredGridBase;

UENUM(BlueprintType)
enum class ETileState : uint8
{
	Free UMETA(DisplayName = "Free"),
	Blocked UMETA(DisplayName = "Blocked"),
	Disabled UMETA(DisplayName = "Disabled")
};


USTRUCT(Blueprintable)
struct GRIDPLUGIN_API FTileStructured
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FIntPoint Coords {0, 0};

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	ETileState State = ETileState::Free;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	AActor* Actor = nullptr;

	bool operator==(FTileStructured const& Other) const
	{
		return Coords == Other.Coords;
	}
};

UINTERFACE(BlueprintType)
class GRIDPLUGIN_API UGridActorInterface : public UInterface
{
	GENERATED_BODY()
};

class GRIDPLUGIN_API IGridActorInterface
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GridActorInterface")
	void OnMovedOnGrid(FIntPoint const& NewTile, FIntPoint const& OldTile);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GridActorInterface")
	void OnRemovedFromGrid(FIntPoint const& OldTile);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GridActorInterface")
	void OnPlacedOntoGrid(FIntPoint const& NewTile);
};

UCLASS(abstract)
class GRIDPLUGIN_API AStructuredGridBase : public AGridBase
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	TArray<FTileStructured> Tiles;

public:
	// This defines the grid dimensions
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FIntPoint NumTiles2D{5, 8};

	// First Index. This way negative indices can be specified.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StartingCoords)
	FIntPoint StartingCoords{0, 0};

public:
#if WITH_EDITOR
	void PostEditChangeProperty(struct FPropertyChangedEvent& Event) override;
#endif

	// INDEX SECTION. Tiles and their materials are spawned into a 1D TArray and ProcMeshComp respectively. They can be accessed by
	// the StorageIndex. The general Gridlayout is commonly 2D. That's were the TileCoords come in. But for e.g. HexCoords the exist
	// some non-trivial shift in the common coordinate system and what would be efficient for storage. That's where two extra
	// transformations GetTileCoords(StorageCoords) and the inverse function come in.

public:
	// Get StorageIndex by the StorageCoords (Storage Coords go from 0 to NumTiles2D.X/.Y)
	UFUNCTION(BlueprintPure)
	int GetStorageIndex(FIntPoint const& StorageCoords) const;

	// Get StorageIndex to address the e.g. the material of a certain tile.
	UFUNCTION(BlueprintPure)
	int GetStorageIndexFromTileCoords(FIntPoint const& TileCoords) const;

protected:
	FIntPoint GetStorageCoords(int StorageIndex) const;

	FIntPoint GetTileCoords(int StorageIndex) const;

	// Transformation from Storage to TileCoords is trivial for many cases. HexMesh need to override that.
	virtual FIntPoint GetTileCoords(FIntPoint const& StorageCoords) const;

	virtual FIntPoint GetStorageCoords(FIntPoint const& TileCoords) const;

	bool AreTileCoordsValid(FIntPoint const& TileCoords) const;
	void ClampTileCoordsToBeValid(FIntPoint& TileCoords) const;
	// END INDEX SECTION

public:
	// HeightFunction which should be overridden for 2D Maps which want to use height profile.
	virtual float GetHeight(FVector2D const& Coords) const;

	virtual TArray<FVector2D> GetTileCorners(FTileStructured const& Tile) const
		PURE_VIRTUAL(AStructuredGridBase::GetTileCorners, return TArray<FVector2D>(););

	UFUNCTION(BlueprintPure)
	virtual FVector2D GetTileCoordsAtLocation(FVector const& Location) const
		PURE_VIRTUAL(AStructuredGridBase::GetTileCoordsAtLocation, return FVector2D(););
	virtual FVector2D GetLocationAtTileCoords2D(FVector2D const& TileCoords) const
		PURE_VIRTUAL(AStructuredGridBase::GetLocationAtTileCoords2D, return FIntPoint(););

	UFUNCTION(BlueprintPure)
	virtual FVector GetLocationAtTileCoords(FVector2D const& TileCoords) const;
	TArray<FVector> GetLocationAtTileCoordsArray(TArray<FVector2D> const& TileCoordArray) const;

	UFUNCTION(BlueprintPure)
	FTileStructured ProjectLocationOntoGrid(FVector const& Location) const;

	// GRID ARITHMETICS SECTION

	UFUNCTION(BlueprintCallable)
	virtual int TileDistance(FIntPoint const& A, FIntPoint const& B) const 
		PURE_VIRTUAL(AStructuredGridBase::TileDistance, return 0;);

	// Returns all tiles with up to given distance from the center tile
	UFUNCTION(BlueprintCallable)
	virtual TArray<FIntPoint> GetTilesInRange(FIntPoint const& Center, int Range, bool SkipBlockedTiles = true) const;

	// Returns all tiles with up to given distance from (0,0)
	virtual TArray<FIntPoint> GetTilesInRange(int Range) const
		PURE_VIRTUAL(AStructuredGridBase::GetTilesInRange, return TArray<FIntPoint>(););

	// Returns all tiles with a distance in the given Range. MinDistance is exclusive, MaxDistance inclusive
	UFUNCTION(BlueprintCallable)
	virtual TArray<FIntPoint> GetTilesInRing(
		FIntPoint const& Center, int MinDistance, int MaxDistance, bool SkipBlockedTiles = true) const;

	UFUNCTION(BlueprintCallable)
	TArray<FIntPoint> GetNeighbours(FIntPoint const& Center, bool SkipBlockedTiles = true) const;

	// BLOCKED TILES LOGIC

	UFUNCTION(BlueprintCallable)
	bool IsTileBlocked(FIntPoint const& TileCoords) const;
	
	UFUNCTION(BlueprintCallable)
	ETileState GetTileState(FIntPoint const& TileCoords) const;

	UFUNCTION(BlueprintCallable)
	AActor* GetActorOnTile(FIntPoint const& TileCoords) const;

	UFUNCTION(BlueprintCallable)
	bool IsActorOnGrid(AActor* Actor, FIntPoint& OutputTileCoordsActorIsOn) const;

	UFUNCTION(BlueprintCallable)
	TArray<AActor*> GetAllActorsOnGrid() const;

	UFUNCTION(BlueprintCallable)
	void SetTileBlocked(FIntPoint const& TileCoords, bool NewBlockValue, AActor* Actor);
	
	UFUNCTION(BlueprintCallable)
	void SetTileState(FIntPoint const& TileCoords, ETileState NewState);

	UFUNCTION(BlueprintCallable)
	void RemoveActorFromGrid(AActor* Actor);
	
	UFUNCTION(BlueprintCallable)
	void ClearTile(FIntPoint const& TileCoords);

	UFUNCTION(BlueprintCallable)
	void PlaceActorOnGrid(FIntPoint const& TileCoords, AActor* Actor);
	
	UFUNCTION(BlueprintCallable)
	void MoveActorOnGrid(FIntPoint const& StartCoords, FIntPoint const& TargetCoords);

	UFUNCTION(BlueprintCallable)
	TArray<FIntPoint> GetTilesByState(ETileState State) const;

	// PATH FINDING SECTION
	TArray<FIntPoint> AStarSearch(FIntPoint const& StartTileCoords, FIntPoint const& TargetTileCoords) const;

	UFUNCTION(BlueprintCallable)
	TArray<FIntPoint> ShortestPath(FIntPoint const& StartTileCoords, FIntPoint const& TargetTileCoords, bool& Success) const;

	// MESH CREATION SECTION

	// Returns Vertices and TriangleIDs for a triangulation of the requested tile.
	void CalcTileTriangulation(int TileNumber, TArray<FVector>& Vertices, TArray<int>& TriangleIDs) const override;

	void CalcGridLineTriangulation(int TileNumber, TArray<FVector>& Vertices, TArray<int>& TriangleIDs) const override;

	UFUNCTION(BlueprintCallable)
	void CalcTileMesh(TArray<FIntPoint> const& TileCoords, TArray<FVector>& Vertices, TArray<int>& TriangleIDs) const;

protected:
	void InitTilesArray() override;

private:
	void DebugPrintGridLabels() override;
	FTileStructured& GetTileRef(FIntPoint const& TileCoords) { return Tiles[GetStorageIndexFromTileCoords(TileCoords)]; }
	FTileStructured const& GetTileRef(FIntPoint const& TileCoords) const  { return Tiles[GetStorageIndexFromTileCoords(TileCoords)]; }
};

inline int AStructuredGridBase::GetStorageIndex(FIntPoint const& StorageCoords) const
{
	return StorageCoords.X + StorageCoords.Y * NumTiles2D.X;
}

inline int AStructuredGridBase::GetStorageIndexFromTileCoords(FIntPoint const& TileCoords) const
{
	return GetStorageIndex(GetStorageCoords(TileCoords));
}

inline FIntPoint AStructuredGridBase::GetStorageCoords(int StorageIndex) const
{
	return FIntPoint(StorageIndex % NumTiles2D.X, StorageIndex / NumTiles2D.X);
}

inline FIntPoint AStructuredGridBase::GetTileCoords(int StorageIndex) const
{
	return GetTileCoords(GetStorageCoords(StorageIndex));
}

inline FIntPoint AStructuredGridBase::GetTileCoords(FIntPoint const& StorageCoords) const
{
	return StorageCoords + StartingCoords;
}

inline FIntPoint AStructuredGridBase::GetStorageCoords(FIntPoint const& TileCoords) const
{
	return TileCoords - StartingCoords;
}

inline bool AStructuredGridBase::AreTileCoordsValid(FIntPoint const& TileCoords) const
{
	FIntPoint StorageCoords = GetStorageCoords(TileCoords);

	return 0 <= StorageCoords.X && StorageCoords.X < NumTiles2D.X && 0 <= StorageCoords.Y && StorageCoords.Y < NumTiles2D.Y;
}

inline void AStructuredGridBase::ClampTileCoordsToBeValid(FIntPoint& TileCoords) const
{
	if (AreTileCoordsValid(TileCoords))
		return;

	FIntPoint StorageCoords = GetStorageCoords(TileCoords);

	FIntPoint ValidStorageCoords =
		FIntPoint(FMath::Clamp(StorageCoords.X, 0, NumTiles2D.X - 1), FMath::Clamp(StorageCoords.Y, 0, NumTiles2D.Y - 1));

	TileCoords = GetTileCoords(ValidStorageCoords);
}

inline float AStructuredGridBase::GetHeight(FVector2D const& Coords) const
{
	return 0;
}

inline TArray<FIntPoint> AStructuredGridBase::GetNeighbours(FIntPoint const& Center, bool SkipBlockedTiles) const
{
	return GetTilesInRange(Center, 1, SkipBlockedTiles);
}

inline bool AStructuredGridBase::IsTileBlocked(FIntPoint const& TileCoords) const
{
	return GetTileState(TileCoords) == ETileState::Blocked;
}

inline ETileState AStructuredGridBase::GetTileState(FIntPoint const& TileCoords) const
{
	if (AreTileCoordsValid(TileCoords))
		return GetTileRef(TileCoords).State;

	return ETileState::Disabled;
}

inline AActor* AStructuredGridBase::GetActorOnTile(FIntPoint const& TileCoords) const
{
	if (AreTileCoordsValid(TileCoords))
		return GetTileRef(TileCoords).Actor;

	return nullptr;
}

inline void AStructuredGridBase::SetTileState(FIntPoint const& TileCoords, ETileState NewState)
{
	if (AreTileCoordsValid(TileCoords))
	{
		GetTileRef(TileCoords).State = NewState;
		
		if (NewState != ETileState::Blocked)
			GetTileRef(TileCoords).Actor = nullptr;

		DrawAllMeshes();
	}
}