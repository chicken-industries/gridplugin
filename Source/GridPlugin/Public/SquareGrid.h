#pragma once

#include "ArrayHelpers.h"
#include "CoreMinimal.h"
#include "StructuredGridBase.h"

#include "SquareGrid.generated.h"

UENUM(BlueprintType)
enum class ESquareGridMetricType : uint8
{
	Edges UMETA(DisplayName = "Edges", ToolTip = "Tiles with a common edge are considered as distance 1"),
	EdgesAndCorners UMETA(
		DisplayName = "Edges and Corners", ToolTip = "Tiles with a common edge or corner are considered as distance 1"),
	Euclidian UMETA(DisplayName = "Euclidian", ToolTip = "Distance between tiles is approximated (or exact) as euclidian distance")
};

// Uses AGridBase::GridScale as square edge length
UCLASS()
class GRIDPLUGIN_API ASquareGrid : public AStructuredGridBase
{
	GENERATED_BODY()

public:
	// Define how distances on this grid are calculated, relevant for, e.g., path finding
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	ESquareGridMetricType GridMetricType{ESquareGridMetricType::Euclidian};

	TArray<FVector2D> GetTileCorners(FTileStructured const& Tile) const override;

	FVector2D GetTileCoordsAtLocation(FVector const& Location) const override;

	FVector2D GetLocationAtTileCoords2D(FVector2D const& TileCoords) const override;

	int TileDistance(FIntPoint const& A, FIntPoint const& B) const override;

	TArray<FIntPoint> GetTilesInRange(int Range) const override;
};

inline TArray<FVector2D> ASquareGrid::GetTileCorners(FTileStructured const& Tile) const
{
	TArray<FVector2D> Corners{FVector2D(-0.5, 0.5), FVector2D(-0.5, -0.5), FVector2D(0.5, -0.5), FVector2D(0.5, 0.5)};
	ArrayHelpers::AddConstToArray(Corners, FVector2D(Tile.Coords));

	return Corners;
}

inline FVector2D ASquareGrid::GetTileCoordsAtLocation(FVector const& Location) const
{
	return FVector2D(Location) / GridScale;
}

inline FVector2D ASquareGrid::GetLocationAtTileCoords2D(FVector2D const& TileCoords) const
{
	return GridScale * TileCoords;
}