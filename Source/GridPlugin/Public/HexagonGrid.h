#pragma once

#include "CoreMinimal.h"
#include "StructuredGridBase.h"

#include "HexagonGrid.generated.h"

constexpr float HexGridBaseVector1_X = UE_SQRT_3;
constexpr float HexGridBaseVector1_Y = 0.f;

constexpr float HexGridBaseVector2_X = UE_HALF_SQRT_3;
constexpr float HexGridBaseVector2_Y = 1.5f;

// Uses AGridBase::GridScale as hexagon edge length
UCLASS()
class GRIDPLUGIN_API AHexagonGrid : public AStructuredGridBase
{
	GENERATED_BODY()

protected:
	FIntPoint GetTileCoords(FIntPoint const& StorageCoords) const override;

	FIntPoint GetStorageCoords(FIntPoint const& TileCoords) const override;

public:
	TArray<FVector2D> GetTileCorners(FTileStructured const& Tile) const override;

	FIntPoint HexRound(FVector2D const& HexCoordsContinuous) const;

	FVector2D GetTileCoordsAtLocation(FVector const& Location) const override;

	FVector2D GetLocationAtTileCoords2D(FVector2D const& TileCoords) const override;

	int TileDistance(FIntPoint const& A, FIntPoint const& B) const override;

	TArray<FIntPoint> GetTilesInRange(int Range) const override;

	TArray<FIntPoint> GetDiagonalNeighbours(FIntPoint const& Coords) const;
};

inline FIntPoint AHexagonGrid::GetTileCoords(FIntPoint const& StorageCoords) const
{
	return Super::GetTileCoords(StorageCoords - FIntPoint(StorageCoords.Y / 2, 0));
}

inline FIntPoint AHexagonGrid::GetStorageCoords(FIntPoint const& TileCoords) const
{
	FIntPoint StorageCoords = Super::GetStorageCoords(TileCoords);
	return StorageCoords + FIntPoint(StorageCoords.Y / 2, 0);
}

inline FVector2D AHexagonGrid::GetTileCoordsAtLocation(FVector const& Location) const
{
	// find the linear combination of location by base vectors, i.e.
	// location = a * base_vector1 + b * base_vector2
	return FVector2D(UE_SQRT_3 * Location.X - Location.Y, 2 * Location.Y) / (3 * GridScale);
}

inline FVector2D AHexagonGrid::GetLocationAtTileCoords2D(FVector2D const& TileCoords) const
{
	return GridScale * (TileCoords.X * FVector2D(HexGridBaseVector1_X, HexGridBaseVector1_Y) +
						   TileCoords.Y * FVector2D(HexGridBaseVector2_X, HexGridBaseVector2_Y));
}

inline int AHexagonGrid::TileDistance(FIntPoint const& A, FIntPoint const& B) const
{
	int dx = A.X - B.X;
	int dy = A.Y - B.Y;

	return (FMath::Abs(dx) + FMath::Abs(dy) + FMath::Abs(dx + dy)) / 2;
}