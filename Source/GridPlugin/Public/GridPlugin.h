// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

#include <chrono>

DECLARE_LOG_CATEGORY_EXTERN(LogGridPlugin, Display, All);

class FGridPluginModule : public IModuleInterface
{
public:
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

inline auto StartTiming()
{
	return std::chrono::system_clock::now();
}

void StopAndLogTiming(std::chrono::system_clock::time_point start);
