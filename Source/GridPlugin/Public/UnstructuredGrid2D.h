#pragma once

#include "CoreMinimal.h"
#include "Graph.h"
#include "GridBase.h"

#include "UnstructuredGrid2D.generated.h"

UCLASS()
class GRIDPLUGIN_API AUnstructuredGrid2D : public AGridBase
{
	GENERATED_BODY()

protected:
	FGraph Graph;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FVector2D> AreaInputLocations;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector2D BorderMin{0.f, 0.f};
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector2D BorderMax{1000.f, 1000.f};

public:
#if WITH_EDITOR
	void PostEditChangeProperty(struct FPropertyChangedEvent& Event) override;
#endif

	// HeightFunction which should be overridden for 2D Maps which want to use height profile.
	virtual float GetHeight(FVector2D const& Coords) const
	{
		return 0;
	}

	TArray<FVector> GetLocationAtTileCoordsArray(TArray<FVector2D> const& TileCoordArray) const;

protected:
	void InitTilesArray() override;

	void CalcGridLineTriangulation(int TileNumber, TArray<FVector>& Vertices, TArray<int>& TriangleIDs) const override;
	void CalcTileTriangulation(int TileNumber, TArray<FVector>& Vertices, TArray<int>& TriangleIDs) const override;
};
