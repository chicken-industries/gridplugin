#pragma once

#include "CoreMinimal.h"
#include "StructuredGridBase.h"

#include "RingGrid.generated.h"

UENUM(BlueprintType)
enum class ERingGridMetricType : uint8
{
	Edges UMETA(DisplayName = "Edges", ToolTip = "Tiles with a common edge are considered as distance 1"),
	EdgesAndCorners UMETA(
		DisplayName = "Edges and Corners", ToolTip = "Tiles with a common edge or corner are considered as distance 1")
};

UCLASS(HideCategories = StartingCoords)
class GRIDPLUGIN_API ARingGrid : public AStructuredGridBase
{
	GENERATED_BODY()

public:
	// Multiplied with GridScale results in real InnerRadius
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float InnerRadius = 5.f;

	// Multiplied with GridScale results in real OuterRadius
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float OuterRadius = 15.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = 0.01f, ClampMax = 360.f))
	float RingCutAngle = 180.f;

	// Set this to true if you want the behaviour of a full circle (with or without 360 degree RingCutAngle).
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool PeriodicityInCircularDirection = false;

	// Define how distances on this grid are calculated, relevant for, e.g., path finding
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	ERingGridMetricType GridMetricType{ERingGridMetricType::Edges};

#if WITH_EDITOR
	void PostEditChangeProperty(struct FPropertyChangedEvent& e);
#endif

protected:
	// Need to deactivate the negative Index on the ring grid. The Mapping is currently not fitted for the negative index.
	using AStructuredGridBase::StartingCoords;

	FIntPoint GetTileCoords(FIntPoint const& StorageCoords) const override;

	FIntPoint GetStorageCoords(FIntPoint const& TileCoords) const override;

public:
	TArray<FVector2D> GetTileCorners(FTileStructured const& Tile) const override;

	FVector2D GetTileCoordsAtLocation(FVector const& Location) const override;

	FVector2D GetLocationAtTileCoords2D(FVector2D const& TileCoords) const override;

	void InitTilesArray() override;

	int TileDistance(FIntPoint const& A, FIntPoint const& B) const override;

	TArray<FIntPoint> GetTilesInRange(int Range) const override;

	TArray<FIntPoint> GetTilesInRange(FIntPoint const& Center, int Range, bool SkipBlockedTiles = true) const override;

private:
	float TileLengthRadial = 10;
	float TileArcWidth = 10;
};

inline FIntPoint ARingGrid::GetTileCoords(FIntPoint const& StorageCoords) const
{
	return StorageCoords;
}

inline FIntPoint ARingGrid::GetStorageCoords(FIntPoint const& TileCoords) const
{
	return TileCoords;
}