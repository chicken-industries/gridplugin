// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "GridPlugin.h"

#define LOCTEXT_NAMESPACE "FGridPluginModule"

void FGridPluginModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
}

void FGridPluginModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FGridPluginModule, GridPlugin)

DEFINE_LOG_CATEGORY(LogGridPlugin)

void StopAndLogTiming(std::chrono::system_clock::time_point start)
{
	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;
	UE_LOG(LogGridPlugin, Display, TEXT("%f seconds"), elapsed_seconds.count());
}
