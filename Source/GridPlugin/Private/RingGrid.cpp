#include "RingGrid.h"

#include "ArrayHelpers.h"

#if WITH_EDITOR
void ARingGrid::PostEditChangeProperty(struct FPropertyChangedEvent& e)
{
	Super::PostEditChangeProperty(e);

	FName MemberPropertyName = (e.MemberProperty != NULL) ? e.MemberProperty->GetFName() : NAME_None;

	// Enforce InnerRadius <= OuterRadius
	if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(ARingGrid, InnerRadius))
		InnerRadius = FMath::Min(InnerRadius, OuterRadius);
	else if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(ARingGrid, OuterRadius))
		OuterRadius = FMath::Max(InnerRadius, OuterRadius);

	if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(ARingGrid, InnerRadius) ||
		MemberPropertyName == GET_MEMBER_NAME_CHECKED(ARingGrid, OuterRadius) ||
		MemberPropertyName == GET_MEMBER_NAME_CHECKED(ARingGrid, RingCutAngle))
		UpdateTilesDataAndMeshes();
}
#endif

TArray<FVector2D> ARingGrid::GetTileCorners(FTileStructured const& Tile) const
{
	TArray<FVector2D> Corners{FVector2D(-0.5, 0.5), FVector2D(0.5, 0.5), FVector2D(0.5, -0.5), FVector2D(-0.5, -0.5)};
	ArrayHelpers::AddConstToArray(Corners, FVector2D(Tile.Coords));

	return Corners;
}

FVector2D ARingGrid::GetTileCoordsAtLocation(FVector const& Location) const
{
	float r = FVector2D(Location).Size();
	float arc = FMath::Atan2(Location.X, Location.Y);

	// We want arc to be in [0,2pi), but Atan2 maps 2nd half of the circle onto [-pi, 0)
	if (arc < 0)
		arc += 2 * PI;

	float x = arc / TileArcWidth - 0.5f;
	float y = ((r / GridScale - InnerRadius) / TileLengthRadial - 0.5f);

	return FVector2D(x, y);
}

FVector2D ARingGrid::GetLocationAtTileCoords2D(FVector2D const& TileCoords) const
{
	float arc = TileArcWidth * (TileCoords.X + 0.5f);
	float r = GridScale * (InnerRadius + TileLengthRadial * (TileCoords.Y + 0.5f));

	return r * FVector2D(sin(arc), cos(arc));
}

void ARingGrid::InitTilesArray()
{
	Super::InitTilesArray();

	TileLengthRadial = (OuterRadius - InnerRadius) / NumTiles2D.Y;
	TileArcWidth = RingCutAngle / NumTiles2D.X / 180 * PI;
}

int ARingGrid::TileDistance(FIntPoint const& A, FIntPoint const& B) const
{
	int dx = FMath::Abs(A.X - B.X);
	int dy = FMath::Abs(A.Y - B.Y);

	if (PeriodicityInCircularDirection)
		dx = FMath::Min(dx, NumTiles2D.X - dx);

	switch (GridMetricType)
	{
		case ERingGridMetricType::Edges:
		default:

			return dx + dy;

		case ERingGridMetricType::EdgesAndCorners:

			return FMath::Max(dx, dy);
	}
}

TArray<FIntPoint> ARingGrid::GetTilesInRange(int Range) const
{
	TArray<FIntPoint> TilesInRange;

	for (int x = -Range; x <= Range; x++)
	{
		int yLimit = (GridMetricType == ERingGridMetricType::Edges) ? Range - FMath::Abs(x) : Range;

		for (int y = -yLimit; y <= yLimit; y++)
			TilesInRange.Add(FIntPoint(x, y));
	}

	return TilesInRange;
}

TArray<FIntPoint> ARingGrid::GetTilesInRange(FIntPoint const& Center, int Range, bool SkipBlockedTiles) const
{
	TArray<FIntPoint> TilesInRangeOfCenter;

	for (FIntPoint const& RelativeNeighbours : GetTilesInRange(Range))
	{
		FIntPoint AbsoluteNeighbour = Center + RelativeNeighbours;

		if (PeriodicityInCircularDirection)
			AbsoluteNeighbour.X = (AbsoluteNeighbour.X + NumTiles2D.X) % NumTiles2D.X;

		if (AreTileCoordsValid(AbsoluteNeighbour))
			if (!SkipBlockedTiles || !IsTileBlocked(AbsoluteNeighbour))
				TilesInRangeOfCenter.Add(AbsoluteNeighbour);
	}

	return TilesInRangeOfCenter;
}