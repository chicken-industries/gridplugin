#include "UnstructuredGrid2D.h"

#include "Polyhedron2D.h"

#if WITH_EDITOR
void AUnstructuredGrid2D::PostEditChangeProperty(struct FPropertyChangedEvent& Event)
{
	Super::PostEditChangeProperty(Event);

	FName MemberPropertyName = (Event.MemberProperty != nullptr) ? Event.MemberProperty->GetFName() : NAME_None;
	FName PropertyName = (Event.Property != nullptr) ? Event.Property->GetFName() : NAME_None;

	UE_LOG(LogTemp, Verbose, TEXT("Changed MemberProperty: %s"), *MemberPropertyName.ToString());
	UE_LOG(LogTemp, Verbose, TEXT("With it's property: %s"), *PropertyName.ToString());

	if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(AUnstructuredGrid2D, AreaInputLocations) ||
		MemberPropertyName == GET_MEMBER_NAME_CHECKED(AUnstructuredGrid2D, BorderMin) ||
		MemberPropertyName == GET_MEMBER_NAME_CHECKED(AUnstructuredGrid2D, BorderMax))
		UpdateTilesDataAndMeshes();
}
#endif

void AUnstructuredGrid2D::InitTilesArray()
{
	Graph = FGraph(BorderMin, BorderMax);

	for (auto const& Location : AreaInputLocations)
		Graph.AddNode(FGraphNode(Location));

	Graph.CreateEdgesByDelaunyTriangulation<FGraphEdge>();

	TilesNum = Graph.GetNodes().Num();
}

void AUnstructuredGrid2D::CalcGridLineTriangulation(int TileNumber, TArray<FVector>& Vertices, TArray<int>& TriangleIDs) const
{
	TArray<FVector2D> VerticesLocal;
	TArray<int> TriangleIDsLocal;

	Graph.GetNodes()[TileNumber]->GetVoronoiCell().ComputeTriangulatedBorders(
		VerticesLocal, TriangleIDsLocal, GridLineThickness <= 1.f, GridLineThickness, GridRefinementLevel, true);
	AddLocalToGlobalMesh(Vertices, TriangleIDs, GetLocationAtTileCoordsArray(VerticesLocal), TriangleIDsLocal);
}

void AUnstructuredGrid2D::CalcTileTriangulation(int TileNumber, TArray<FVector>& Vertices, TArray<int>& TriangleIDs) const
{
	TArray<FVector2D> VerticesLocal;
	TArray<int> TriangleIDsLocal;
	Graph.GetNodes()[TileNumber]->GetVoronoiCell().ComputeTriangulation(VerticesLocal, TriangleIDsLocal, GridRefinementLevel);

	AddLocalToGlobalMesh(Vertices, TriangleIDs, GetLocationAtTileCoordsArray(VerticesLocal), TriangleIDsLocal);
}

TArray<FVector> AUnstructuredGrid2D::GetLocationAtTileCoordsArray(TArray<FVector2D> const& TileCoordArray) const
{
	TArray<FVector> Locations;
	Locations.Reserve(TileCoordArray.Num());

	for (FVector2D const& TileCoord : TileCoordArray)
		Locations.Add(FVector(TileCoord, GetHeight(TileCoord)));

	return Locations;
}