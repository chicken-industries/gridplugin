#include "HexagonGrid.h"

#include "ArrayHelpers.h"

constexpr float ONETHIRD = 1.f / 3.f;
constexpr float TWOTHIRD = 2.f / 3.f;

TArray<FVector2D> AHexagonGrid::GetTileCorners(FTileStructured const& Tile) const
{
	TArray<FVector2D> Corners{FVector2D(ONETHIRD, ONETHIRD), FVector2D(-ONETHIRD, TWOTHIRD), FVector2D(-TWOTHIRD, ONETHIRD),
		FVector2D(-ONETHIRD, -ONETHIRD), FVector2D(ONETHIRD, -TWOTHIRD), FVector2D(TWOTHIRD, -ONETHIRD)};

	ArrayHelpers::AddConstToArray(Corners, FVector2D(Tile.Coords));

	return Corners;
}

// Game World Locations <-> Hex Grid
// https://www.redblobgames.com/grids/hexagons/#distances-cube
FIntPoint AHexagonGrid::HexRound(FVector2D const& HexCoordsContinuous) const
{
	// virtual z variable needed for this algorithm, see "cube coordinates"
	float z = -HexCoordsContinuous.X - HexCoordsContinuous.Y;

	int round_x = round(HexCoordsContinuous.X);
	int round_y = round(HexCoordsContinuous.Y);
	int round_z = round(z);

	float x_diff = FMath::Abs(round_x - HexCoordsContinuous.X);
	float y_diff = FMath::Abs(round_y - HexCoordsContinuous.Y);
	float z_diff = FMath::Abs(round_z - z);

	if (x_diff > y_diff && x_diff > z_diff)
		round_x = -round_y - round_z;
	else if (y_diff > z_diff)
		round_y = -round_x - round_z;

	return FIntPoint(round_x, round_y);
}

TArray<FIntPoint> AHexagonGrid::GetTilesInRange(int Range) const
{
	TArray<FIntPoint> HexCoordsWithinRange;

	for (int x = -Range; x <= Range; x++)
		for (int y = FMath::Max(-Range, -x - Range); y <= FMath::Min(Range, -x + Range); y++)
			HexCoordsWithinRange.Add(FIntPoint(x, y));

	return HexCoordsWithinRange;
}

TArray<FIntPoint> AHexagonGrid::GetDiagonalNeighbours(FIntPoint const& Coords) const
{
	TArray<FIntPoint> DiagonalNeighbours;
	
	TArray<FIntPoint> NeighboursOfOrigin{{1,1}, {2,-1}, {1, -2}, {-1,-1}, {-2,1}, {-1,2}};

	for (FIntPoint NeighbourRelative : NeighboursOfOrigin)
	{
		FIntPoint NeighbourAbsolute = Coords + NeighbourRelative;

		if (AreTileCoordsValid(NeighbourAbsolute))
			DiagonalNeighbours.Add(NeighbourAbsolute);
	}

	return DiagonalNeighbours;
}
