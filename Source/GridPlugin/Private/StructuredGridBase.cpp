#include "StructuredGridBase.h"

#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Polyhedron2D.h"

#if WITH_EDITOR
void AStructuredGridBase::PostEditChangeProperty(struct FPropertyChangedEvent& Event)
{
	Super::PostEditChangeProperty(Event);

	FName MemberPropertyName = (Event.MemberProperty != nullptr) ? Event.MemberProperty->GetFName() : NAME_None;

	if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(AStructuredGridBase, NumTiles2D) ||
		MemberPropertyName == GET_MEMBER_NAME_CHECKED(AStructuredGridBase, StartingCoords))
		UpdateTilesDataAndMeshes();
}
#endif

void AStructuredGridBase::CalcTileTriangulation(int TileNumber, TArray<FVector>& Vertices, TArray<int>& TriangleIDs) const
{
	if (Tiles[TileNumber].State == ETileState::Disabled)
		return;
	
	FPolyhedron2D Poly(GetTileCorners(Tiles[TileNumber]));

	TArray<FVector2D> VerticesLocal;
	TArray<int> TriangleIDsLocal;

	Poly.ComputeTriangulation(VerticesLocal, TriangleIDsLocal, GridRefinementLevel);

	AddLocalToGlobalMesh(Vertices, TriangleIDs, GetLocationAtTileCoordsArray(VerticesLocal), TriangleIDsLocal);
}

void AStructuredGridBase::CalcGridLineTriangulation(int TileNumber, TArray<FVector>& Vertices, TArray<int>& TriangleIDs) const
{
	if (Tiles[TileNumber].State == ETileState::Disabled)
		return;
	
	FPolyhedron2D Poly(GetTileCorners(Tiles[TileNumber]));

	TArray<FVector2D> VerticesLocal;
	TArray<int> TriangleIDsLocal;

	bool RelativeGridLineSize = GridLineThickness <= 2.f;

	Poly.ComputeTriangulatedBorders(
		VerticesLocal, TriangleIDsLocal, RelativeGridLineSize, RelativeGridLineSize ? GridLineThickness : GridLineThickness / GridScale, GridRefinementLevel, true);

	AddLocalToGlobalMesh(Vertices, TriangleIDs, GetLocationAtTileCoordsArray(VerticesLocal), TriangleIDsLocal);
}

void AStructuredGridBase::CalcTileMesh(TArray<FIntPoint> const& TileCoords, TArray<FVector>& Vertices, TArray<int>& TriangleIDs) const
{
	for (FIntPoint const& TileCoord : TileCoords)
		CalcTileTriangulation(GetStorageIndexFromTileCoords(TileCoord), Vertices, TriangleIDs);
}

void AStructuredGridBase::InitTilesArray()
{
	Tiles.Init(FTileStructured(), NumTiles2D.X * NumTiles2D.Y);

	for (int i_y = 0; i_y != NumTiles2D.Y; ++i_y)
		for (int i_x = 0; i_x != NumTiles2D.X; ++i_x)
		{
			FIntPoint StorageCoords = FIntPoint(i_x, i_y);
			int StorageIndex = GetStorageIndex(StorageCoords);
			Tiles[StorageIndex].Coords = GetTileCoords(StorageCoords);
		}

	TilesNum = Tiles.Num();
}

FTileStructured AStructuredGridBase::ProjectLocationOntoGrid(FVector const& Location) const
{
	FIntPoint TileCoords = GetTileCoordsAtLocation(Location).IntPoint();
	ClampTileCoordsToBeValid(TileCoords);

	return GetTileRef(TileCoords);
}

FVector AStructuredGridBase::GetLocationAtTileCoords(FVector2D const& TileCoords) const
{
	FVector2D Location = GetLocationAtTileCoords2D(TileCoords);
	return FVector(Location, GetHeight(Location));
}

TArray<FVector> AStructuredGridBase::GetLocationAtTileCoordsArray(TArray<FVector2D> const& TileCoordArray) const
{
	TArray<FVector> Locations;
	Locations.Reserve(TileCoordArray.Num());

	for (FVector2D const& TileCoord : TileCoordArray)
		Locations.Add(GetLocationAtTileCoords(TileCoord));

	return Locations;
}

TArray<FIntPoint> AStructuredGridBase::GetTilesInRange(FIntPoint const& Center, int Range, bool SkipBlockedTiles) const
{
	TArray<FIntPoint> TilesInRangeOfCenter;

	for (FIntPoint const& RelativeNeighbours : GetTilesInRange(Range))
	{
		FIntPoint AbsoluteNeighbour = Center + RelativeNeighbours;

		if (AreTileCoordsValid(AbsoluteNeighbour))
			if (!SkipBlockedTiles || GetTileState(AbsoluteNeighbour) == ETileState::Free)
				TilesInRangeOfCenter.Add(AbsoluteNeighbour);
	}

	return TilesInRangeOfCenter;
}

TArray<FIntPoint> AStructuredGridBase::GetTilesInRing(
	FIntPoint const& Center, int MinDistance, int MaxDistance, bool SkipBlockedTiles) const
{
	TArray<FIntPoint> TilesInRing = GetTilesInRange(Center, MaxDistance, SkipBlockedTiles);

	for (FIntPoint const& InnerPoint : GetTilesInRange(Center, MinDistance, SkipBlockedTiles))
		TilesInRing.Remove(InnerPoint);

	return TilesInRing;
}

bool AStructuredGridBase::IsActorOnGrid(AActor* Actor, FIntPoint& OutputTileCoordsActorIsOn) const
{
	if (Actor == nullptr)
		return false;
	
	for (FTileStructured const& Tile : Tiles)
		if (Tile.Actor == Actor)
		{
			OutputTileCoordsActorIsOn = Tile.Coords;
			return true;
		}

	return false;
}

TArray<AActor*> AStructuredGridBase::GetAllActorsOnGrid() const
{
	TArray<AActor*> ActorsOnGrid;
	
	for (FTileStructured const& Tile : Tiles)
		if (Tile.State == ETileState::Blocked && Tile.Actor)
			ActorsOnGrid.Add(Tile.Actor);

	return ActorsOnGrid;
}


void AStructuredGridBase::SetTileBlocked(FIntPoint const& TileCoords, bool NewBlockValue, AActor* Actor)
{
	if (AreTileCoordsValid(TileCoords))
	{
		auto& Tile = GetTileRef(TileCoords);
		Tile.State = NewBlockValue ? ETileState::Blocked : ETileState::Free;
		Tile.Actor = NewBlockValue ? Actor : nullptr;
	}
}

void AStructuredGridBase::RemoveActorFromGrid(AActor* Actor)
{
	FIntPoint OldTileCoordsOfActor;
	
	if (IsActorOnGrid(Actor, OldTileCoordsOfActor))
	{
		SetTileBlocked(OldTileCoordsOfActor, false, nullptr);

		if (Actor->GetClass()->ImplementsInterface(UGridActorInterface::StaticClass()))
			IGridActorInterface::Execute_OnRemovedFromGrid(Actor, OldTileCoordsOfActor);
	}
}

void AStructuredGridBase::ClearTile(FIntPoint const& TileCoords)
{
	auto Actor = GetActorOnTile(TileCoords);

	SetTileBlocked(TileCoords, false, nullptr);

	if (Actor)
		if (Actor->GetClass()->ImplementsInterface(UGridActorInterface::StaticClass()))
			IGridActorInterface::Execute_OnRemovedFromGrid(Actor, TileCoords);
}

void AStructuredGridBase::PlaceActorOnGrid(FIntPoint const& TileCoords, AActor* Actor)
{
	if (Actor == nullptr)
		return;

	FIntPoint CurrentActorCoords;
	if (IsActorOnGrid(Actor, CurrentActorCoords))
	{
		MoveActorOnGrid(CurrentActorCoords, TileCoords);
	}
	else
	{
		SetTileBlocked(TileCoords, true, Actor);
		Actor->SetActorLocation(GetLocationAtTileCoords(TileCoords) + GetActorLocation());

		if (Actor->GetClass()->ImplementsInterface(UGridActorInterface::StaticClass()))
			IGridActorInterface::Execute_OnPlacedOntoGrid(Actor, TileCoords);
	}
}

void AStructuredGridBase::MoveActorOnGrid(FIntPoint const& StartCoords, FIntPoint const& TargetCoords)
{
	if (auto Actor = GetActorOnTile(StartCoords))
	{
		SetTileBlocked(StartCoords, false, nullptr);
		SetTileBlocked(TargetCoords, true, Actor);

		Actor->SetActorLocation(GetLocationAtTileCoords(TargetCoords) + GetActorLocation());

		if (Actor->GetClass()->ImplementsInterface(UGridActorInterface::StaticClass()))
			IGridActorInterface::Execute_OnMovedOnGrid(Actor, TargetCoords, StartCoords);
	}
}


TArray<FIntPoint> AStructuredGridBase::GetTilesByState(ETileState State) const
{
	TArray<FIntPoint> TilesMatchingState;

	for (FTileStructured const& Tile : Tiles)
		if (Tile.State == State)
			TilesMatchingState.Add(Tile.Coords);

	return TilesMatchingState;
}

// Help structure for the AStar algorithm
struct FIntPointWithCosts
{
	FIntPointWithCosts(FIntPoint Coords, int TrackedCostFromStart, int HeuristicCostToTarget)
		: Coords(Coords), TrackedCostFromStart(TrackedCostFromStart), HeuristicCostToTarget(HeuristicCostToTarget)
	{
		CostSum = TrackedCostFromStart + HeuristicCostToTarget;
	}

	FIntPoint Coords;
	int TrackedCostFromStart;
	int HeuristicCostToTarget;
	int CostSum;
};

TArray<FIntPoint> AStructuredGridBase::AStarSearch(FIntPoint const& StartTileCoords, FIntPoint const& TargetTileCoords) const
{
	TArray<FIntPointWithCosts> Queue;
	Queue.Add(FIntPointWithCosts(StartTileCoords, 0, TileDistance(StartTileCoords, TargetTileCoords)));

	TArray<FIntPoint> Visited;

	TArray<FIntPoint> Predecessors;
	Predecessors.Init(FIntPoint(-1, -1), TilesNum);

	TArray<int> CostSoFar;
	CostSoFar.Init(INT_MAX, TilesNum);
	CostSoFar[GetStorageIndexFromTileCoords(StartTileCoords)] = 0;

	while (Queue.Num() > 0)
	{
		Queue.Sort([](FIntPointWithCosts const& A, FIntPointWithCosts const& B) {
			return (A.CostSum != B.CostSum) ? (A.CostSum < B.CostSum) : (A.HeuristicCostToTarget < B.HeuristicCostToTarget);
		});

		FIntPointWithCosts Current = Queue[0];

		if (Current.Coords == TargetTileCoords)
			break;

		Queue.RemoveAt(0);
		Visited.Add(Current.Coords);

		for (FIntPoint const& Neighbour : GetNeighbours(Current.Coords))
		{
			int TotalCostToReachNeighbour = Current.TrackedCostFromStart + TileDistance(Neighbour, Current.Coords);

			int StorageIndexNeighbour = GetStorageIndexFromTileCoords(Neighbour);

			if (!Visited.Contains(Neighbour) && TotalCostToReachNeighbour < CostSoFar[StorageIndexNeighbour])
			{
				CostSoFar[StorageIndexNeighbour] = TotalCostToReachNeighbour;
				Queue.Add(FIntPointWithCosts(Neighbour, TotalCostToReachNeighbour, TileDistance(Neighbour, TargetTileCoords)));
				Predecessors[StorageIndexNeighbour] = Current.Coords;
				// storage index should always be valid, since validity is checked in the GetAllHexesWithinRangeOfOriginHex()
			}
		}
	}

	return Predecessors;
}

TArray<FIntPoint> AStructuredGridBase::ShortestPath(
	FIntPoint const& StartTileCoords, FIntPoint const& TargetTileCoords, bool& Success) const
{
	TArray<FIntPoint> Path;

	if (!AreTileCoordsValid(StartTileCoords) || !AreTileCoordsValid(TargetTileCoords))
	{
		Success = false;
		return Path;
	}

	TArray<FIntPoint> Predecessors = AStarSearch(StartTileCoords, TargetTileCoords);

	Success = true;
	FIntPoint Current = TargetTileCoords;

	while (StartTileCoords != Current)
	{
		if (AreTileCoordsValid(Current))
		{
			Path.Add(Current);
			Current = Predecessors[GetStorageIndexFromTileCoords(Current)];
		}
		else
		{
			Success = false;
			break;	  // no valid path found. return value of path will be start & end
		}
	}

	Path.Add(StartTileCoords);

	return Path;
}

void AStructuredGridBase::DebugPrintGridLabels()
{
	if (!ShowGridLabels)
		return;

	FlushDebugStrings(GetWorld());

	for (FIntPoint const& Tile : GetTilesByState(ETileState::Free))
	{
		FString const RenderString = FString::Printf(TEXT("(%i,%i)"), Tile.X, Tile.Y);
		DrawDebugString(GetWorld(), GetLocationAtTileCoords(Tile), RenderString, this, FColor::White);
	}
}