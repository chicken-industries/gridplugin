#include "GridBase.h"

#include "ArrayHelpers.h"
#include "Components/SceneComponent.h"
#include "DrawDebugHelpers.h"
#include "ProceduralMeshComponent.h"
#include "GridPlugin.h"

AGridBase::AGridBase()
{
	PrimaryActorTick.bCanEverTick = true;

	CustomSceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	RootComponent = CustomSceneRoot;
	
	TilesCollisionMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("TilesMeshes"));
	TilesCollisionMesh->SetupAttachment(RootComponent);
	TilesCollisionMesh->SetVisibility(false);

	GridLineMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GridLines"));
	GridLineMesh->SetupAttachment(RootComponent);

	HighlightMeshes = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("HighlightMeshes"));
	HighlightMeshes->SetupAttachment(RootComponent);
}

void AGridBase::RaiseGridMeshSlightlyAboveTileMesh()
{
	// Stacking the meshes
	// Top - Gridlines
	// Mid - Highlights
	// Bottom - Tiles
	GridLineMesh->SetRelativeLocation(FVector(0, 0, 2));
	HighlightMeshes->SetRelativeLocation(FVector(0, 0, 1));
}

void AGridBase::Tick(float DeltaSeconds)
{
	DebugPrintGridLabels();
}

void AGridBase::BeginPlay()
{
	Super::BeginPlay();
	RaiseGridMeshSlightlyAboveTileMesh();
	InitTilesArray();
}

void AGridBase::PostActorCreated()
{
	Super::PostActorCreated();
	RaiseGridMeshSlightlyAboveTileMesh();
	UpdateTilesDataAndMeshes();
}

#if WITH_EDITOR
void AGridBase::PostEditChangeProperty(FPropertyChangedEvent& Event)
{
	Super::PostEditChangeProperty(Event);

	FName MemberPropertyName = Event.MemberProperty ? Event.MemberProperty->GetFName() : NAME_None;

	if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(AGridBase, GridRefinementLevel) ||
		MemberPropertyName == GET_MEMBER_NAME_CHECKED(AGridBase, GridScale))
		UpdateTilesDataAndMeshes();
	else if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(AGridBase, GridLineThickness))
		DrawGridLinesMesh();
}
#endif

void AGridBase::UpdateTilesDataAndMeshes()
{
	InitTilesArray();
	DrawAllMeshes();
}

void AGridBase::DrawAllMeshes()
{
	DrawGridLinesMesh();
	DrawTilesMeshes();
}

void AGridBase::DrawGridLinesMesh()
{
	TArray<int> TriangleIDs;
	TArray<FVector> Vertices;

	auto start = StartTiming();

	for (int i = 0; i < TilesNum; ++i)
		CalcGridLineTriangulation(i, Vertices, TriangleIDs);

	GridLineMesh->CreateMeshSection(0, Vertices, TriangleIDs, {}, {}, {}, {}, false);

	UE_LOG(LogGridPlugin, Display, TEXT("Grid Metrics - Number of Tiles: %i"), TilesNum);
	UE_LOG(LogGridPlugin, Display, TEXT("GridLineMesh - Vertices: %i, Triangles: %i"), Vertices.Num(), TriangleIDs.Num() / 3);
	StopAndLogTiming(start);
}

void AGridBase::DrawTilesMeshes()
{
	TilesCollisionMesh->ClearAllMeshSections();

	TArray<int> TriangleIDs;
	TArray<FVector> Vertices;

	auto start = StartTiming();

	for (int i = 0; i < TilesNum; ++i)
		CalcTileTriangulation(i, Vertices, TriangleIDs);

	TilesCollisionMesh->CreateMeshSection(0, Vertices, TriangleIDs, {}, {}, {}, {}, true);

	UE_LOG(LogGridPlugin, Display, TEXT("Tile Meshes - Vertices: %i, Triangles: %i"), Vertices.Num(), TriangleIDs.Num() / 3);
	StopAndLogTiming(start);
}

void AGridBase::AddLocalToGlobalMesh(TArray<FVector>& GlobalVertices, TArray<int>& GlobalTriangleIDs,
	TArray<FVector> const& LocalVertices, TArray<int>& LocalTriangleIDs)
{
	ArrayHelpers::AddConstToArray(LocalTriangleIDs, GlobalVertices.Num());
	GlobalTriangleIDs.Append(LocalTriangleIDs);
	GlobalVertices.Append(LocalVertices);
}

void AGridBase::DebugPrintGridLabels()
{
}
