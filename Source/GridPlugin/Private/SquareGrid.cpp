#include "SquareGrid.h"

int ASquareGrid::TileDistance(FIntPoint const& A, FIntPoint const& B) const
{
	int dx = FMath::Abs(A.X - B.X);
	int dy = FMath::Abs(A.Y - B.Y);

	switch (GridMetricType)
	{
		case ESquareGridMetricType::Edges:
			// Manhatten metric

			return dx + dy;

		case ESquareGridMetricType::EdgesAndCorners:
			// Neighbours over edges and corners are equal distant

			return FMath::Max(dx, dy);

		case ESquareGridMetricType::Euclidian:
		default:
			// Simplified version of euclidian distance on square grid See Sebastian Lagues A* Implementation
			// https://www.youtube.com/watch?v=mZfyt03LDH4

			if (dx > dy)
				return 14 * dy + 10 * (dx - dy);
			else
				return 14 * dx + 10 * (dy - dx);
	}
}

TArray<FIntPoint> ASquareGrid::GetTilesInRange(int Range) const
{
	TArray<FIntPoint> TilesInRange;

	switch (GridMetricType)
	{
		case ESquareGridMetricType::Edges:

			for (int x = -Range; x <= Range; x++)
			{
				int yLimit = Range - FMath::Abs(x);

				for (int y = -yLimit; y <= yLimit; y++)
					TilesInRange.Add(FIntPoint(x, y));
			}
			break;

		case ESquareGridMetricType::EdgesAndCorners:

			for (int x = -Range; x <= Range; x++)
				for (int y = -Range; y <= Range; y++)
					TilesInRange.Add(FIntPoint(x, y));

			break;

		case ESquareGridMetricType::Euclidian:
		default:

			// GetNeighbours() calls this function with Range=1
			int xLimit = FMath::Max(1, Range / 10);

			for (int x = -xLimit; x <= xLimit; x++)
				for (int y = -xLimit; y <= xLimit; y++)
					if (TileDistance(FIntPoint(0, 0), FIntPoint(x, y)) <= FMath::Max(14, Range))
						TilesInRange.Add(FIntPoint(x, y));
	}

	return TilesInRange;
}